#include "natfork.ih"

void NatFork::setupDaemonMsg()
{
    if (d_options.log().empty())
    {
        imsg.off();
        return;
    }
                                // verbose msg via multiStreambuf
    if (d_options.verbose())
        imsg.reset(&d_multiStreambuf);
    else
        imsg.off();
}
