#include "rotatingstreambuf.ih"

                                            // receives --log (or --log-data)
void RotatingStreambuf::open(string const &name, bool stdLog)
{
    d_name = name;
    d_overflow = &RotatingStreambuf::unlockedOverflow;

    Exception::open(d_out, name, ios::in | ios::ate, ios::out);

    if (d_header and file_size(name) == 0)  // the std. log files don't have
        (*d_header)(d_out);                 // header lines

    if (stdLog)
        s_stdRotate.rotationStreambuf = this;
    else                   
        s_dataRotate.rotationStreambuf = this;
}
