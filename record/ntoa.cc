#include "record.ih"

// static
string Record::ntoa(uint32_t ipAddr)
{
    String::Type type;
    auto vec{ String::split(&type, inet_ntoa({ ipAddr }), String::TOK, ".") };

    ostringstream os;
    os << setfill('0');

    for (auto const &el: vec)
        os << setw(3) << el << '.';

    string ret{ os.str() };
    ret.resize(ret.length() - 1);

    return ret;
}
