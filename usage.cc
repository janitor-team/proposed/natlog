//                     usage.cc

#include "main.ih"

namespace {

    char const config[] = R"(
Where:
   [options] - optional arguments (short options between parentheses):
      --config (-c) path - path: pathname of the configuration file
                           (default: `)"; 


    char const ctCommand[] = R"(')
      --conntrack-bytecounts: the `conntrack' program also reports the
                           number of sent and received bytes  
      --conntrack-command cmd - cmd: `conntrack' program call (default 
                           `)"; 


    char const ctDevice[] = R"(')
      --conntrack-device dev - dev: `conntrack' data device
                           (default `)"; 

    char const noDaemon[] = R"(')
      --conntrack-ip-header-size size - size: assumed IP header sizes
                           (default: 0, commonly encountered: 20)
      --conntrack-restart max - max number of times `conntrack' is
                           restarted if prematurely ended
                           (default 10 restarts)
      --debug            - log debug messages (unless --log off)
      --help (-h)        - provide this help
      --log type         - log information about connections.
                           Specify `type' as:
                               syslog: (default) log messages using syslog
                               off:    no messages are logged
                               path:   messages are logged to the 'path'
      --log-data path    - write log-data on the file `path'
      --log-rotate spec  - rotate log- and log-data files. `Spec' is either
                           `time[mhd]' or `time[mhd]nFiles' (m: minutes,
                           h: hours, d: days. By default (or if 'time' or
                           `nfiles' are zero) log files are not rotated.
      --no-bytes         - sent and received numbers of bytes are not listed 
                           in log-entries
      --no-daemon        - do not run )"; 

    char const pidFile[] = R"( in the background
      --no-dst           - do not include 'dst' entries in the log file
                           (default: 'dst' entries are included)
      --no-via           - do not include 'via' entries in the log file
                           (default: 'via' entries are included)
      --pid-file (-p) path - `path' is the name of the file holding the
                           daemon's PID
                           (default `)";

    char const protocol[] = R"(')
      --protocol (-P) spec - spec: protocols handled by )"; 


    char const optionRot[] = R"(Use colon separated combinations of
                           tcp (default), udp and icmp, or use `all'
      --rotate           - the log-files of a separately running process are 
                           rotated
      --rotate-data      - the log-data files of a separately running )";

    char const optionS[] = R"( process are rotated
      -S                 - systemd fix: specify this as first argument when
                           starting )"; 

    char const syslogTag[] = R"( from natlog.service
      --stdout (-s)      - write syslog-equivalent messages to the std output
                            (implied by --verbose; only with --no-daemon)
      --syslog-tag id    - id: identifier prefixed to syslog messages
                           (default `)"; 

    char const syslogFacil[] = R"(')
      --syslog-facility fac  - fac: used syslog facility
                           (default `)"; 

    char const syslogPri[] = R"(')
      --syslog-priority pri  - pri: used syslog priority
                           (default `)"; 

    char const ttl[] = R"(')
      --terminate        - end a running natlog program using the PID found
                           in the PID-file (see option --pid-file)
      --time (-t) spec   - time specification:
                               raw:   (default) seconds since the epoch;
                               utc:   date and time in UTC
                               local: local date and time
      --ttl (-T) secs    - time-to-live for received connections. Format:
                           `<secs>u' (UDP/ICMP ttl) and/or `<secs>t'
                           (TCP ttl). Default: )"; 



    char const trailer[] = R"(t
                           info about a connection is logged if it is not
                           active for `secs' seconds
      --verbose (-V)     - write additional information:
                               with --no-daemon: to stdout;
                               with daemons and --no-syslog not specified:
                                                 to the syslog daemon
      --version (-v)     - show version information and terminate
                            (suppressed by --no-daemon)
   command   - command to execute:
       conntrack:  use `conntrack' to find the source-nat connections
       in out:     in, out: names of network devices (e.g., eth0)
                   directly capture packets on the `in' and `out' devices
                   in: source natting is applied to its source addresses
                   out: the (natting) device connecting to the
                        destination addresses
       in inAddr inMask out outAddr outMask:
                   in, out: names of files written by tcpdump
                   (e.g., tcpdump -i device -w filename)
                   `in': in-device packages (source natting was applied to
                         `in')
                   `out' packages of the device connecting to the destination 
                         addresses  
                   `inAddr inMask': address and netmask of the in-device
                   `outAddr outMask': address and netmask of the out-device
)";

}


void usage(std::string const &progname)
{
    cout << "\n" <<
        progname << " by " << Icmbuild::author << "\n" <<
        progname << " V" << Icmbuild::version << " " << 
                            Icmbuild::years << "\n"
        "\n"
        "Usage: " << progname << " [options] command" <<
        config      << Options::defaultConfigPath() << 
        ctCommand   << Options::defaultConntrackCommand() << ' ' <<
                       Options::defaultConntrackArgs() <<
        ctDevice    << Options::defaultConntrackDevice() << 
        noDaemon    << progname <<
        pidFile     << Options::defaultPIDfile() << 
        protocol    << progname <<
        optionRot   << progname <<
        optionS     << progname <<
        syslogTag   << Options::defaultSyslogIdent() << 
        syslogFacil << Options::defaultSyslogFacility() << 
        syslogPri   << Options::defaultSyslogPriority() << 
        ttl         << Options::TTL << 'u' << Options::TTL_TCP << 
        trailer;

}
