#include "ipbase.ih"

    // logs TCP and UDP records
void IPbase::logConnection(Record const &record) const
{
    if (record.viaIP() != 0)
    {
        d_stdMsg << "from " << record.beginTime() << 
            " thru " << record.endTime() << 
            ShowSeconds::utcMarker() << ": " << 
            record.protocolStr() << ' ' <<
            record.sourceIPstr() << ',' << 
                                left << setw(5) << record.sourcePort();

        (this->*d_via)(record);
        (this->*d_dst)(record);

        d_stdMsg << right;

        (this->*d_byteCounts)(record);

        d_stdMsg << s_logTypeText[s_logType].first << endl;

        logCSV(record);         // calls *d_logData -> logData()
    }
}
