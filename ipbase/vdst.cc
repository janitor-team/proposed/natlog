#include "ipbase.ih"

void IPbase::vDst(Record const &record) const
{
        d_stdMsg << " "
                    "to " << record.destIPstr() << ',' <<  
                                showLeft(record.destPort(), ';');
}
