#include "rotatingstream.ih"

RotatingStream::RotatingStream(void (*header)(std::ostream &))
:
    RotatingStreambuf(header),
    std::ostream(this)
{}
        
