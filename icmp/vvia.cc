#include "icmp.ih"

// overrides
void ICMP::vVia(Record const &record) const
{
    stdMsg() << " (via: " << record.viaIPstr()  << ") "  "     ";
}
