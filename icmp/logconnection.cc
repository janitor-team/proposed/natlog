#include "icmp.ih"

void ICMP::logConnection(Record const &record) const
{
    stdMsg() << "from " << record.beginTime() << 
                " thru " << record.endTime() << 
                ShowSeconds::utcMarker() << ": icmp " <<
                record.sourceIPstr() << "      ";

    (this->*d_via)(record);
    (this->*d_dst)(record);
    (this->*d_byteCounts)(record);

    stdMsg() << logType().first << endl;

    logCSV(record);
}
