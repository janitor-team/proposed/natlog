#ifndef INCLUDED_PRODUCERSIGNALS_
#define INCLUDED_PRODUCERSIGNALS_

#include <bobcat/signal>

#include "../producer/producer.h"

class ProducerSignals: public Producer, public FBB::SignalHandler
{
    public:
        ProducerSignals();
};
        
#endif
