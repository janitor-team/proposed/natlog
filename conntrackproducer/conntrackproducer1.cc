#include "conntrackproducer.ih"

ConntrackProducer::ConntrackProducer(ostream &stdMsg, Storage &storage)
:
    d_options(Options::instance()),
    d_stdMsg(stdMsg),
    d_storage(storage),
    d_tcpUdp(&ConntrackProducer::notUsed)
{
    if (access(d_options.conntrackDevice(), R_OK) != 0)
        throw Exception{} << "Cannot read " << d_options.conntrackDevice();

    if (d_options.byteCounts())
        activateByteCounts();

    setTcpUdp(IP_Types::TCP, "TCP");
    setTcpUdp(IP_Types::UDP, "UDP");

    if (not d_options.hasProtocol(IP_Types::ICMP))
        d_icmp = &ConntrackProducer::notUsed;
    else
    {
        imsg << "conntrack reports ICMP connections" << endl;
        d_icmp = &ConntrackProducer::icmp;
    }
      
}
