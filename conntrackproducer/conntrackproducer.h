#ifndef INCLUDED_CONNTRACKPRODUCER_
#define INCLUDED_CONNTRACKPRODUCER_

#include <iosfwd>

#include <bobcat/pattern>
#include <bobcat/fork>
#include <bobcat/pipe>

#include "../producersignals/producersignals.h"

class Options;
class Storage;

namespace FBB
{
    class Pattern;
}

class ConntrackProducer: public ProducerSignals, public FBB::Fork
{
    FBB::Pipe d_pipe;
    Options &d_options;
    std::ostream &d_stdMsg;
    Storage &d_storage;

    bool d_signaled = false;

    bool (ConntrackProducer::*d_tcpUdp)(std::string const &line);
    bool (ConntrackProducer::*d_icmp)(std::string const &line);

    static FBB::Pattern s_tcpudp;
    static FBB::Pattern s_icmp;
    static char const s_nfConntrackAcct[];

    public:
        ConntrackProducer(std::ostream &stdMsg, Storage &storage);
        ~ConntrackProducer();

    private:
        void activateByteCounts() const;
        bool icmp(std::string const &line);     // process ICMP connections
        bool notUsed(std::string const &line);  // false: used for unused 
                                                //        connection types
        void process(std::string const &line);  // process the conntrack info
        void setTcpUdp(size_t protocolType,     // set d_tcpUdp for TCP/UDP
                       char const *label);
        bool tcpUdp(std::string const &line);   // process TCP/UDP connections

        void childProcess()                 override;
        void childRedirections()            override;
        void parentProcess()                override;
        void parentRedirections()           override;
        void run()                          override;
        void signalHandler(size_t signum)   override;
};
        
#endif









