#include "conntrackproducer.ih"

    // false is returned when line does not contain TCP or UDP info
    //      (or, via notUsed, if neither TCP or UDP should be logged)
    // true is otherwise returned. If the udp or tcp protocol is handled 
    // then handled protocol records are pushed into the storage 

bool ConntrackProducer::tcpUdp(string const &line)
{
    if (not (s_tcpudp << line))     // if this is not an UDP or TCP connection
        return false;

    if (                            // if UDP/TCP is requested: push it
        d_options.hasProtocol(
                        s_tcpudp[ static_cast<size_t>(CTtcpudp::PROTOCOL) ] 
                             ) 
    )
        d_storage.push( new ConntrackRecord{ Record::TCP, s_tcpudp } );

    return true;                    // see process()
}
