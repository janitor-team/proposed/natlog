#include "options.ih"

void Options::setConntrack()
{
    if (not d_arg.option(&d_conntrackDevice, "conntrack-device"))
        d_conntrackDevice = s_defaultConntrackDevice;


    if (not d_arg.option(&d_conntrackCommand, "conntrack-command"))
        (d_conntrackCommand = s_defaultConntrackCommand) += 
                                                 s_defaultConntrackArgs; 

        // when multiple protocols are specified, conntrack's command does
        // not receive a -p setting, but the conntrack records only handle
        // the specified protocols.
    d_conntrackCommand +=           // by default: all protocols, otherwise
            (                       // use the specified protocol
                d_protocols.size() == 1 ? 
                    " -p " + 
                        s_protocol2name.find(*d_protocols.begin())->second
                :
                    ""s
            );

    string value;
    if (d_arg.option(&value, "conntrack-restart"))
        d_conntrackRestart = stoul(value);

    if (d_arg.option(&value, "conntrack-ip-header-size"))
        d_IPheaderSize = stoul(value);
}
